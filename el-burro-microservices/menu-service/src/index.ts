import "reflect-metadata";
import { createConnection } from "typeorm";
import express from "express";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { MenuResolver } from "./resolvers/MenuResolver";

(async () => {
  const app = express();

  await createConnection();

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [MenuResolver]
    }),
    context: ({ req, res }) => ({ req, res })
  });


  apolloServer.applyMiddleware({
     app:app,
     path:'/Menu'
    ,cors: false
  });

  app.listen(4000, () => {
    console.log("express server started http://localhost:4000/Menu");
  });


})();
