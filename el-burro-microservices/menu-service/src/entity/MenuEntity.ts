import { Entity, Column, BaseEntity, ObjectIdColumn, ObjectID, UpdateDateColumn, CreateDateColumn, Unique } from "typeorm";
import { Field, ID, ObjectType } from "type-graphql";
import { IsNotEmpty } from "class-validator";

@ObjectType()
@Entity()
@Unique(['name'])
export class MenuEntity extends BaseEntity {

  @Field(() => ID)
  @ObjectIdColumn()
  _id: ObjectID;

  @Field()
  @Column()
  @IsNotEmpty({ message: 'The type is required' })
  type: string;
 
  @Field()
  @Column()
  @IsNotEmpty({ message: 'The name is required' })
  name: string;

  @Field()
  @Column()
  @IsNotEmpty({ message: 'The description is required' })
  description: string;

  @Field()
  @Column()
  @IsNotEmpty({ message: 'The price is required' })
  price: number;

  @Field({ nullable: true })
  @Column()
  image: string;

  @Field({ nullable: true , defaultValue:false})
  @Column()
  isSpecial: boolean;

  @Field({ nullable: true })
  @Column()
  @IsNotEmpty({ message: 'The order is required' })
  order: number;

  @Field()
  @Column()
  @IsNotEmpty({ message: 'The createdBy is required' })
  createdBy: string;

  @Field({ nullable: true })
  @Column()
  modifiedBy: string;

  @Field()
  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date

  @Field({ nullable: true })
  @UpdateDateColumn({ type: 'timestamp', nullable: true })
  updatedAt?: Date

}

