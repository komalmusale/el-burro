import { MenuEntity } from "../entity/MenuEntity";
import { Arg, Field, InputType, Mutation, Query, Resolver } from "type-graphql";
import { ObjectID } from "typeorm";
import { IsNotEmpty} from "class-validator";

@InputType()
export class MenuInput {
  @Field()
  @IsNotEmpty({ message: 'The type is required' })
  type: string;
 
  @Field()
  @IsNotEmpty({ message: 'The name is required' })
  name: string;

  @Field()
  @IsNotEmpty({ message: 'The description is required' })
  description: string;

  @Field()
  @IsNotEmpty({ message: 'The price is required' })
  price: number;

  @Field({ nullable: true })
  image: string;

  @Field({ nullable: true , defaultValue:false})
  isSpecial: boolean;

  @Field({ nullable: true })
  @IsNotEmpty({ message: 'The order is required' })
  order: number;

  @Field()
  @IsNotEmpty({ message: 'The createdBy is required' })
  createdBy: string;

  @Field({ nullable: true })
  modifiedBy: string;
}

@InputType()
export class MenuUpdateInput {

  @Field({ nullable: true })
  type: string;
  
  @Field({ nullable: true })
  name: string;

  @Field({ nullable: true })
  description: string;

  @Field({ nullable: true })
  price: number;

  @Field({ nullable: true })
  image: string;

  @Field({ nullable: true})
  isSpecial: boolean;

  @Field({ nullable: true })
  order: number;

  @Field()
  modifiedBy: string;

}

@Resolver()
export class MenuResolver {
  @Mutation(() => MenuEntity)
  async createMenu(@Arg("options", () => MenuInput) options: MenuInput) {
    const menu = await MenuEntity.create(options).save();
    return menu;
  }

  @Mutation(() => Boolean)
  async updateMenu(
    @Arg("_id", () => String) _id: ObjectID,
    @Arg("input", () => MenuUpdateInput) input: MenuUpdateInput
  ) {
    await MenuEntity.update(_id, input);
    return true;
  }

  @Query(() => [MenuEntity])
  menus() {
    return MenuEntity.find({order:{type:"DESC", order:"ASC"}});
  }

  @Query(() => [MenuEntity])
  menuSpecial() {
    return MenuEntity.find({where:{isSpecial:true}, order:{type:"DESC", order:"ASC"}});
  }
  @Query(() => [MenuEntity])
  ourmenuName() {
    return MenuEntity.find({ select:["type"],  });
  }

  @Query(() => MenuEntity)
  menuit(@Arg("_id", () => String) _id: ObjectID,) {
    return MenuEntity.findOne(_id);
  }


}

