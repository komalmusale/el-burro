import { ObjectType, Field, ID } from "type-graphql"
import { Entity, BaseEntity, ObjectID, ObjectIdColumn } from "typeorm"

@ObjectType()
@Entity()
export class WorkingEntity extends BaseEntity {
    
    @Field(() => ID)
    @ObjectIdColumn()
    _id: ObjectID;

    @Field()
    day: string

    @Field()
    startTime: string

    @Field()
    endTime: string
}