import { Entity, Column, BaseEntity, ObjectIdColumn, ObjectID, CreateDateColumn, UpdateDateColumn, Unique } from "typeorm";
import { Field, ID, ObjectType } from "type-graphql";
import { IsEmail, IsNotEmpty } from "class-validator";
import { WorkingEntity } from "./WorkingEntity";
@ObjectType()
@Entity()
@Unique(['name'])
export class RestaurantEntity extends BaseEntity {
  @Field(() => ID)
  @ObjectIdColumn()
  _id: ObjectID;

  @Field()
  @Column()
  @IsNotEmpty({ message: 'The name is required' })
  name: string;

  @Field()
  @Column()
  @IsNotEmpty({ message: 'The address is required' })
  address: string;

  @Field({ nullable: true })
  @Column()
  @IsEmail({}, { message: 'Incorrect email' })
  email: string;

  @Field({ nullable: true })
  @Column()
  phone: string;

  @Field({ nullable: true })
  @Column()
  image: string;

  @Field({ nullable: true })
  @Column()
  description: string;

  @Field({ nullable: true })
  @Column()
  latitude: string;

  @Field({ nullable: true })
  @Column()
  longitude: string;

  @Field(()=>[WorkingEntity])
  @Column()
  working:[WorkingEntity];

  @Field({ nullable: true })
  @Column()
  createdBy: string;

  @Field({ nullable: true })
  @Column()
  modifiedBy: string;

  @Field()
  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date

  @Field({ nullable: true })
  @UpdateDateColumn({ type: 'timestamp', nullable: true })
  updatedAt?: Date

}
