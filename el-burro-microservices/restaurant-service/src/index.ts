import "reflect-metadata";
import { createConnection } from "typeorm";
import express from "express";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { Restaurant_Resolver } from "./resolvers/RestaurantResolver";

(async () => {
  const app = express();

  await createConnection();

  const apolloServer = new ApolloServer({
    schema: await buildSchema({ 
      resolvers: [Restaurant_Resolver]
    }),
    context: ({ req, res }) => ({ req, res })
  });

  apolloServer.applyMiddleware({
     app:app,
     path:'/Restaurant'
    ,cors: false
  });

  app.listen(5000, () => {
    console.log("express server started http://localhost:5000/Restaurant");
  });


})();
