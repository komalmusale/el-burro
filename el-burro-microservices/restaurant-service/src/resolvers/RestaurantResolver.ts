import { IsNotEmpty } from "class-validator";
import { Resolver, Mutation, Arg, Query, InputType, Field } from "type-graphql";
import { ObjectID } from "typeorm";
import { RestaurantEntity } from "../entity/RestaurantEntity";
@InputType()
class RestaurantsInput {

  @Field()
  @IsNotEmpty({ message: 'The name is required' })
  name: string;

  @Field()
  @IsNotEmpty({ message: 'The address is required' })
  address: string;

  @Field({ nullable: true })
  email: string;

  @Field({ nullable: true })
  phone: string;

  @Field({ nullable: true })
  image: string;

  @Field({ nullable: true })
  description: string;

  @Field({ nullable: true })
  latitude: string;

  @Field({ nullable: true })
  longitude: string;

  @Field(()=>[WorkingInput])
  working:[WorkingInput];

  @Field()
  createdBy: string;

  @Field({ nullable: true })
  modifiedBy: string;
}

@InputType()
class WorkingInput {
  @Field()
  day: string

  @Field()
  startTime: string

  @Field()
  endTime: string
}

@InputType()
class RestaurantsUpdateInput {

  @Field({ nullable: true })
  name: string;

  @Field({ nullable: true })

  address: string;

  @Field({ nullable: true })
  email: string;

  @Field({ nullable: true })
  phone: string;

  @Field({ nullable: true })
  image: string;

  @Field({ nullable: true })
  description: string;

  @Field({ nullable: true })
  latitude: string;

  @Field({ nullable: true })
  longitude: string;

  @Field(()=>[WorkingInput])
  working:[WorkingInput];

  @Field({ nullable: true })
  createdBy: string;

  @Field()
  modifiedBy: string;

}

@Resolver()
export class Restaurant_Resolver {
  @Mutation(() => RestaurantEntity)
  async createRestaurant(@Arg("options", () => RestaurantsInput) options: RestaurantsInput) {
    const restaurant = await RestaurantEntity.create(options).save();
    return restaurant;
  }
  @Mutation(() => Boolean)
  async updateRestaurant(
    @Arg("_id", () => String) _id: ObjectID,
    @Arg("input", () => RestaurantsUpdateInput) input: RestaurantsUpdateInput
  ) {
    await RestaurantEntity.update(_id, input);
    return true;
  }
  @Query(() => [RestaurantEntity])
  restaurants() {
    return RestaurantEntity.find();
  }
  @Query(() => RestaurantEntity)
  restaurant(@Arg("_id", () => String) _id: ObjectID,) {
    return RestaurantEntity.findOne(_id);
  }

}
